library(plyr)

set.seed(1)
n = 50
d <- data.frame(year = rep(2000:2002, each = 50), month = sample(12,3*n,replace=TRUE),
                count = round(runif(3*n, 0, 20)))
print(d)

library(plyr)

ddply(d,"year",function(x) c(mean_count = mean(x$count),sd_count = sd(x$count)))

ddply(d,c("year","month"),function(x) c(mean_count = mean(x$count),sd_count = sd(x$count)))