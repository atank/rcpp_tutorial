#include <RcppArmadillo.h>
#include <Rcpp.h>
using namespace Rcpp;
// Below is a simple example of exporting a C++ function to R. You can
// source this function into an R session using the Rcpp::sourceCpp 
// function (or via the Source button on the editor toolbar)

// For more on using Rcpp click the Help button on the editor toolbar

// [[Rcpp::export]]
int timesTwo(int x) {
   return x * 2;
}

// [[Rcpp::export]]
int fibonacci(const int x) {
if (x == 0) return(0);
if (x == 1) return(1);
return (fibonacci(x - 1)) + fibonacci(x - 2);
}

// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::export]]
int add(int x, int y, int z,arma::mat h) {
  int sum = x + y + z;
  return(sum);
}

// [[Rcpp::export]]
int big_for_loop(int n) {
  int total = 0;
  for (int i = 0; i < n;i++) {
    for (int j = 0; j < n;j++) {
      total += i - j;
    }
  }
  return(total);
}





